# Proyecto Final - Bootcamp DevOps I

Proyecto académico con el objetivo de aplicar los aspectos, responsabilidades, y fases del mundo DevOps ([enunciado](./Enunciado-Proyecto_Final.pdf)).

## Herramientas

Se han utilizado los siguientes sistemas operativos y herramientas en la realización de los ejemplos:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [Visual Studio](https://visualstudio.microsoft.com/es/vs/) 2019.
  * [Git](https://www.git-scm.com/) 2.18.0.windows.1.
  * [VirtualBox](https://www.virtualbox.org/) 6.0.10.
  * [Vagrant](https://www.vagrantup.com/) 2.2.5.
* [Debian 9](https://www.debian.org/index.es.html).
  * [Google Cloud SDK](https://cloud.google.com/sdk/install) 264.0.0.
    * alpha 2019.09.22.
    * beta 2019.0.22.
    * bq 2.0.47.
    * core 2019.09.22.
    * gsutil 4.42.
    * kubectl 2019.09.22.
  * [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) 19.03.2, build 6a30dfca03.
  * [Docker Compose](https://docs.docker.com/compose/install/) 1.24.1, build 4667896b.
  * [Git](https://www.git-scm.com/) 2.11.0.

## Contenido

* Aplicación [netcore-counter](https://gitlab.com/aureliopn/netcore-counter).
* [Ejecución local simple](./README_PARTE_02_LOCAL_SIMPLE.md).
* [Ejecución local Docker](./README_PARTE_03_LOCAL_DOCKER.md).
* [Ejecución Google Kubernetes Engine](./README_PARTE_04_GKE.md).
* [Referencias](./README_PARTE_05_REFERENCIAS.md).

## Vídeo

Este es el vídeo del proyecto final, requerido en el enunciado de la práctica:

[![Vídeo proyecto final - Bootcamp DevOps - KeepCoding](http://img.youtube.com/vi/6jX6L5uzSWU/0.jpg)](https://www.youtube.com/watch?v=6jX6L5uzSWU&list=PLm1x63h7CnB2C4jhDLoie5Yihi0q9S1WG&index=2&t=1s)

## Problemas conocidos

* La duración del vídeo excede los 10 minutos. Para "evitar" el problema, se ha subido a YouTube, para que se pueda reproducir a 1.5 de velocidad :stuck_out_tongue_winking_eye:.
* En la narración del vídeo [03_02_local_docker-compose](./video/03_02_local_docker-compose), se dice que "ahora, vamos a utilizar Docker-compose, para levantar un contenedor con Redis, y otro con nuestra aplicación". No es exacto, ya que se levanta un contenedor Redis, y otros tres contenedores con instancias de nuestra aplicación netcore-counter.
* El audio del vídeo es defectuoso en los primeros minutos, debido a un mal contacto del micrófono. Se puede corregir volviendo a componer el vídeo final, tras re-grabar el audio en los siguientes vídeos:

  * [00_introduccion.mkv](./video/00_Introduccion.mkv).
  * [01_netcore-counter.mkv](./video/01_netcore-counter.mkv).
  * [02_01_local_vm.mkv](./video/02_01_local_vm.mkv).
  * [02_02_local_vm.mkv](./video/02_02_local_vm.mkv).

## Tintero

Algunos aspectos que están disponibles en este proyecto, pero que no han sido mostrados en el vídeo, son los siguientes:

* Health Checks: en la [práctica del módulo 05](https://gitlab.com/aureliopn/05_calidad) se utilizaron, se podrían haber añadido en este proyecto.
* Consulta de Logs: al igual que se ha visto la recolección de logs con Kibana, ejecutando con Vagrant, se podrían haber consultado los que se recolectan en GKE.
* Circuit Breaker: se podría haber "tirado" el deployment de la base de datos Redis en GKE, y ver cómo actúa el sistema de resiliencia de la aplicación netcore-counter, consultando los logs, al igual que se hizo en la [práctica del módulo 05](https://gitlab.com/aureliopn/05_calidad).

## Agradecimientos

No aparezco en el vídeo del proyecto, pero no es por vergüenza, es por [deuda técnica](https://en.wikipedia.org/wiki/Technical_debt). Para que se me vea en acción, y para no aumentar los minutos del vídeo del proyecto, hice este vídeo de agradecimientos aparte:

[![Vídeo agradecimientos proyecto final - Bootcamp DevOps - KeepCoding](http://img.youtube.com/vi/3FkBCFgPLm0/0.jpg)](https://youtu.be/3FkBCFgPLm0)

## Tomas falsas (agradecimientos)

Como guinda final para terminar el curso, y como demostración de que "hasta lo más sencillo lleva su tiempo", dejo este vídeo de tomas falsas, para despedir el curso con unas buenas risas :wink: :

[![Vídeo tomas falsas agradecimientos proyecto final - Bootcamp DevOps - KeepCoding](http://img.youtube.com/vi/5kgDKlYNW1M/0.jpg)](https://youtu.be/5kgDKlYNW1M)
