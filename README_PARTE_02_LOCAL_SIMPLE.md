# Proyecto Final - Bootcamp DevOps I - Ejecución Local Simple

Se puede probar la ejecución de la aplicación localmente utilzando:

* Una máquina virtual para probar la ejecución de la aplicación manualmente.
* Un script con Vagrant para probar la ejecución de la aplicación automáticamente.

## Máquina Virtual Debian 9

Actualizar lista de paquetes e instalar Git:

```bash
sudo apt-get update && sudo apt-get install git -y
```

Clonar este repositorio:

```bash
git clone https://gitlab.com/aureliopn/08_proyecto-final
```

Ejecutar el script que instala los requisitos, clona el repositorio de netcore-counter, compila, establece la configuración, y ejecuta netcore-counter:

```bash
cd 08_proyecto-final/local/vm
chmod 700 *.sh
./vm.sh
```

En caso de encontrar algún problema, se puede estudiar la salida generada en el archivo [log.txt](./local/vm/log.txt).

En otro terminal se puede ejecutar:

```bash
curl -X GET "http://127.0.0.1:5000/api/v1/Counter/incrementar" -H "accept: text/plain"
```

Pulsar Ctrl+C para terminar la ejecución de la prueba.

## Vagrant

Se ha comprobado correctamente el despliegue de la infraestructura en un sistema operativo Windows 10, con las siguientes aplicaciones previamente instaladas:

* [Git](https://www.git-scm.com/) 2.18.0.windows.1.
* [VirtualBox](https://www.virtualbox.org/) 6.0.10.
* [Vagrant](https://www.vagrantup.com/) 2.2.5.

Clonar este repositorio:

```powershell
mkdir ejec_local
cd ejec_local
git clone https://gitlab.com/aureliopn/08_proyecto-final
cd 08_proyecto-final
```

Levantar infraestructura con Vagrant:

```powershell
cd local\vagrant
.\ejecutar.ps1
cd ..\..
```

En caso de encontrar algún problema, se puede estudiar la salida generada en los archivos [log_elk.txt](./local/vagrant/log_elk.txt) y [log_counter.txt](./local/vagrant/log_counter.txt).

Credenciales de Kibana:

* Usuario: "admin".
* Contraseña: "hola".
