# Proyecto Final - Bootcamp DevOps I - Ejecución Local Docker

Se puede probar la ejecución de la aplicación localmente utilizando:

* Docker.
* Docker-compose.

## Prerrequisitos

Instalar Docker añadiendo el usuario "devops" al grupo "docker":

```bash
sudo apt-get update && sudo apt-get install git curl apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update && sudo apt-get install docker-ce -y
sudo usermod -aG docker devops
exit
```

Clonar repositorio de netcore-counter:

```bash
git clone https://gitlab.com/aureliopn/netcore-counter
```

## Docker

Construir imagen Docker de netcore-counter, y arrancar contenedor Docker de la imagen construida:

```bash
cd netcore-counter/solution
docker build -t counter .
cd ../..
```

Arrancar contenedor Docker de la imagen construida:

```bash
docker run -it --rm -p 5010:5000 --name counter_5010 counter
```

En otro terminal se puede consultar la documentación ejecutando:

```bash
curl "http://127.0.0.1:5010/api/v1/swagger/index.html" && echo
```

Pulsar Ctrl+C para terminar la ejecución de la prueba.

## Docker-compose

Instalar Docker-compose:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Ejecutar la aplicación con Docker-compose:

```bash
cd netcore-counter/solution
docker-compose up --build
```

En otro terminal se puede ejecutar:

```bash
curl -X GET "http://127.0.0.1:5010/api/v1/Counter/incrementar" -H "accept: text/plain" && echo
```

Pulsar Ctrl+C para terminar la ejecución de la prueba.

Borrar los recursos utilizados:

```bash
docker-compose rm -f
cd ../..
```
