# Proyecto Final - Bootcamp DevOps I - Despliegue en Google Kubernetes Engine

## Prerrequisitos

Instalar Git:

```bash
sudo apt-get update && sudo apt-get install git -y
```

Clonar este repositorio:

```bash
git clone https://gitlab.com/aureliopn/08_proyecto-final
```

Ejecutar el [script para instalar los requisitos](./gke/instalar_requisitos.sh):

```bash
cd 08_proyecto-final/gke
chmod 700 *.sh
./instalar_requisitos.sh
cd ../..
```

## GKE

Crear un proyecto en GKE y habilitar Google Kubernetes Engine. Después, inicializar Google Cloud SDK (ver [regiones](https://cloud.google.com/compute/docs/regions-zones/)):

```bash
gcloud init
```

Ejecutar el [script para crear el clúster](./gke/crear_cluster.sh):

```bash
cd 08_proyecto-final/gke
sudo chmod 700 *.sh
./crear_cluster.sh >> log.txt
cd ../..
```

Se puede ver la evolución del proceso en otro terminal con:

```bash
tail -f log.txt
```

En caso de encontrar algún problema, se puede estudiar la salida generada en el archivo [log.txt](./gke/log.txt).

Credenciales de Grafana:

* Usuario: "admin".
* Contraseña: "prom-operator".

Credenciales de Kiali:

* Usuario: "admin".
* Contraseña: "admin".

Importar [Dashboard](./gke/monitoring/dashboard.json) en Grafana.

## GitLab CI

Documentación:

* [https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster)

Obtener dirección del clúster:

```bash
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
```

Obtener el certificado CA del clúster:

```bash
kubectl get secret $(kubectl get secrets | grep default-token | awk '{print $1};') -o jsonpath="{['data']['ca\.crt']}" && echo
```

Obtener el token de la cuenta de servicio de GitLab "gitlab-admin-token":

```bash
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin-token | awk '{print $1}')
```
