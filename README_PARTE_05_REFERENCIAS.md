# Proyecto Final - Bootcamp DevOps I - Referencias

## Prácticas

* [02 - Administración de Sistemas y Redes](https://gitlab.com/aureliopn/02_sysadmin).
* [03 - Migración a la Nube](https://gitlab.com/aureliopn/03_migranube).
* [04 - Contenedores, más que VMs](https://gitlab.com/aureliopn/04_contenedores).
* [05 - Creando Soluciones de Calidad](https://gitlab.com/aureliopn/05_calidad).
* [06 - Ciclo de Vida de un Desarrollo: CI/CD](https://gitlab.com/aureliopn/06_ciclodesa).
* [07 - Liberando Productos: Medir Impacto y Gestionar Incidencias](https://gitlab.com/aureliopn/07_libeproduc).
* [08 - Proyecto Final](https://gitlab.com/aureliopn/08_proyecto-final).

## Enlaces

* 01 - Agile.
  * [Trello.com](https://trello.com/).
  * [Toyota](https://www.youtube.com/watch?v=Vjdil2nBCf0).
  * [Lean Toyota](https://www.youtube.com/watch?v=VSX3L_RoFpA).
  * [Battleships](https://www.boxuk.com/battleships/).
  * [The Power of Words](https://www.youtube.com/watch?v=Hzgzim5m7oU).
  * [Scrum.org](https://www.scrum.org/).
  * [Scrum Master - Funny](https://www.youtube.com/watch?v=P6v-I9VvTq4&t=190s).
  * [Root Cause Analysis From Juran](https://www.youtube.com/watch?v=IETtnK7gzlE).
  * [Leadership From A Dancing Guy](https://www.youtube.com/watch?v=hO8MwBZl-Vc).
  * [Scrum Guides](https://scrumguides.org/).
  * [Mountain Goat Software](https://www.mountaingoatsoftware.com/agile).
  * [Gantt Charts vs Burn-Down Charts](https://www.youtube.com/watch?time_continue=1&v=U_jwpInVXD8).
  * [Why Do Managers Hate Agile?](https://www.forbes.com/sites/stevedenning/2015/01/26/why-do-managers-hate-agile/).
* 02 - Administración de Sistemas y Redes.
  * [Packer.io](https://packer.io/).
  * [IP Calculator / IP Subnetting](http://jodies.de/ipcalc).
  * [Regex Tutorial](https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285).
  * [File Security](https://www.ics.uci.edu/computing/linux/file-security.php).
  * [Vim](https://vim.rtorr.com/).
  * [Evolución Distribuciones Linux](https://futurist.se/gldt/wp-content/uploads/10.03/gldt1003.svg).
  * [Python Shebang Recommendation](https://www.python.org/dev/peps/pep-0394/#recommendation).
  * [TCP Three-Way Handshake (TCP Synchronization)](http://www.omnisecu.com/tcpip/tcp-three-way-handshake.php).
  * [TCP Terminate Connection Gracefully](http://www.omnisecu.com/tcpip/tcp-connection-termination.php).
  * [TCP Connection Status](https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.1.0/com.ibm.zos.v2r1.halu101/constatus.htm).
  * [Apache Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-16-04).
  * [Nginx Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04-quickstart).
  * [Wireshark](https://www.wireshark.org/download.html).
  * [Ejemplos LVM](https://access.redhat.com/documentation/es-es/red_hat_enterprise_linux/6/html/logical_volume_manager_administration/lvm_examples).
  * [Logstash for Docker](https://www.elastic.co/guide/en/logstash/current/docker-config.html).
  * [Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/7.2/docker.html).
  * [Kibana on Docker](https://www.elastic.co/guide/en/kibana/current/docker.html).
  * [Filebeat on Docker](https://www.elastic.co/guide/en/beats/filebeat/current/running-on-docker.html).
  * [Beats](https://www.elastic.co/es/products/beats).
  * [PostgreSQL Vagrant](https://github.com/vtomasr5/our-postgresql-setup).
  * [How to Install ELK Stack on Debian 9](https://www.rosehosting.com/blog/how-to-install-the-elk-stack-on-debian-9/).
  * [Vagrant Box debian/stretch64](https://app.vagrantup.com/debian/boxes/stretch64).
  * [Vagrant Shell Scripts](https://www.vagrantup.com/docs/provisioning/shell.html).
  * [Linux Performance](http://www.brendangregg.com/linuxperf.html).
  * [Node Exporter Monitoring Disk I/O](https://medium.com/schkn/monitoring-disk-i-o-on-linux-with-the-node-exporter-f8ff8b5249e0).
  * [30 Linux System Monitoring Tools](https://www.cyberciti.biz/tips/top-linux-monitoring-tools.html).
  * [Monitoring Processes Supervisord](https://serversforhackers.com/c/monitoring-processes-with-supervisord).
  * [Install ELK](https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-ubuntu-16-04).
  * [Install Filebeat](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html).
  * [Linux Hardening Guide](https://github.com/trimstray/the-practical-linux-hardening-guide/blob/master/README.md).
  * [Filebeat and Logstash](https://medium.com/tensult/log-centralization-using-filebeat-and-logstash-11640f77cf70).
* 03 - Migración a la Nube.
  * [Azure Stack](https://azure.microsoft.com/en-us/overview/azure-stack/).
  * [Qwiklabs](https://google.qwiklabs.com/).
  * [Google Cloud Platform](https://cloud.google.com/).
  * [Calculadora GCP](https://cloud.google.com/products/calculator/).
  * [Google Cloud Problems](https://downdetector.com/status/google-cloud/map/).
  * [Cloud SQL MySQL](https://cloud.google.com/sql/docs/mysql/quickstart-proxy-test).
  * [Vision AI](https://cloud.google.com/vision/).
  * [Google Images Download Python Script](https://github.com/hardikvasa/google-images-download).
  * [Globally Autoscaling Web Service Compute Engine](https://cloud.google.com/compute/docs/tutorials/globally-autoscaling-a-web-service-on-compute-engine).
* 04 - Contenedores, más que VMs.
  * [Docker Toolbox](https://docs.docker.com/toolbox/overview/).
  * [Docker ENTRYPOINT & CMD](https://blog.eduonix.com/software-development/docker-entrypoint-cmd-docker/).
  * [Names-generator.go](https://github.com/moby/moby/blob/master/pkg/namesgenerator/names-generator.go).
  * [Dockerfile ADD vs COPY](https://devopsheaven.com/docker/dockerfile/add/copy/devops/tar/2017/10/03/dockerfile-add-vs-copy.html).
  * [Multi-Stage Builds](https://training.play-with-docker.com/multi-stage/).
  * [Optimised Docker Images Multi-Stage Builds](https://www.katacoda.com/courses/docker/multi-stage-builds).
  * [Network Container Documentation](https://docs.docker.com/engine/reference/run/#network-container).
  * [Securing System Docker Guides](https://runnable.com/docker/securing-your-docker-system).
  * [DevOps with Docker](https://docker-hy.github.io/).
  * [Docker Compose Reference](https://docs.docker.com/compose/compose-file/).
  * [CKA Expert Certification](https://www.cncf.io/certification/cka/).
  * [CKAD Kubernetes Application Developer](https://www.cncf.io/certification/ckad/).
  * [Kubernetes Cloud Comparison](https://docs.google.com/spreadsheets/d/1U0x4-NQegEPGM7eVTKJemhkPy18LWuHW5vX8uZzqzYo/edit#gid=0).
  * [GCP ping](http://www.gcping.com/).
  * [Kubectl Usage Conventions](https://kubernetes.io/docs/reference/kubectl/conventions/).
  * [CKAD Exercises](https://github.com/dgkanatsios/CKAD-exercises).
  * [K8s cert-manager Documentation](https://docs.cert-manager.io/en/latest/getting-started/install/kubernetes.html).
  * [Nip.io](https://nip.io/).
  * [Helm](https://helm.sh/docs/using_helm/#installing-helm).
  * [OperatorHub.io Operators Registry](https://operatorhub.io/).
  * [Awesome Operators](https://github.com/operator-framework/awesome-operators).
  * [Six Strategies for Application Deployment](https://thenewstack.io/deployment-strategies/).
  * [Zero Downtime Deployment](https://www.youtube.com/watch?v=jGW8-OBvubc).
  * [Kiali: Service mesh observability and configuration](https://www.kiali.io/).
  * [Locust: A modern load testing framework](https://locust.io/).
  * [Dive: Exploring Layers Docker Images](https://github.com/wagoodman/dive).
* 05 - Creando Soluciones de Calidad.
  * [DevOps Topologies](https://web.devopstopologies.com/).
  * [The Twelve-Factor App](https://12factor.net/es/).
  * [RPC gRPC](https://grpc.io/).
  * [Skaffold](https://skaffold.dev/).
  * [Circuit Breaker: App-vNext/Polly Wiki](https://github.com/App-vNext/Polly/wiki/Circuit-Breaker).
  * [Draft: Streamlined Kubernetes Development](https://draft.sh/).
  * [Telepresence](https://www.telepresence.io/).
  * [Okteto: Kubernetes for Developers](https://okteto.com/).
  * [SonarQube](https://www.sonarqube.org/).
  * [Selenium](https://www.seleniumhq.org/).
  * [Distributed Application Runtime (Dapr)](https://cloudblogs.microsoft.com/opensource/2019/10/16/announcing-dapr-open-source-project-build-microservice-applications/).
  * [Jaeger: open source, end-to-end distributed tracing](https://www.jaegertracing.io/).
  * [Installing Istio on GKE](https://cloud.google.com/istio/docs/istio-on-gke/installing).
  * [Deploying Istio on GKE](https://medium.com/@tufin/test-7daa5ee3782b).
  * [Istio & Ingress Gateways](https://istio.io/docs/tasks/traffic-management/ingress/ingress-control/).
  * [Kiali GKE](https://kubekub.com/blog/2019/07/22/managed-istio-on-gke/).
* 06 - Ciclo de Vida de un Desarrollo: CI/CD.
  * [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/).
  * [GitFlow Harmful](https://www.endoflineblog.com/gitflow-considered-harmful).
  * [GitHub Flow](https://githubflow.github.io/).
  * [Understanding GitHub Flow](https://guides.github.com/introduction/flow/index.html).
  * [Deploying at GitHub](https://github.blog/2012-08-29-deploying-at-github/).
  * [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).
  * [Git DMZ Flow](https://gist.github.com/djspiewak/9f2f91085607a4859a66).
  * [Trunk Based Development](https://trunkbaseddevelopment.com/).
  * [Jenkins: Scripted Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/#scripted-pipeline).
  * [Jenkins: Declarative Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/#declarative-pipeline).
  * [gitlab/dind](https://hub.docker.com/r/gitlab/dind/).
  * [Kaniko: Build Container Images In Kubernetes](https://github.com/GoogleContainerTools/kaniko).
  * [Using Docker with Jenkins Pipeline](https://jenkins.io/doc/book/pipeline/docker/).
  * [Swarm Plugin Jenkins](https://wiki.jenkins.io/display/JENKINS/Swarm+Plugin).
  * [Administering Jenkins Wiki](https://wiki.jenkins.io/display/JENKINS/Administering+Jenkins).
  * [Architecting for Scale Jenkins](https://jenkins.io/doc/book/architecting-for-scale/).
  * [Using Docker Images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html).
  * [Pipeline Syntax Jenkins](https://jenkins.io/doc/book/pipeline/syntax/).
  * [Pipeline Steps Reference Jenkins](https://jenkins.io/doc/pipeline/steps/).
  * [Jenkins Pipeline Basic Steps](https://jenkins.io/doc/pipeline/steps/workflow-basic-steps/).
  * [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ce/ci/yaml/README.html).
  * [GitLab Runners on GKE](https://medium.com/ovrsea/how-to-manually-install-and-configure-gitlab-runners-on-gke-google-kubernetes-engine-b81c6c945313).
  * [Recording Tests and Artifacts Jenkins](https://jenkins.io/doc/pipeline/tour/tests-and-artifacts/).
  * [JUnit Test Reports](https://docs.gitlab.com/ce/ci/junit_test_reports.html).
  * [Terraform](https://www.terraform.io/downloads.html).
  * [gruntwork-io/terraform-google-gke](https://github.com/gruntwork-io/terraform-google-gke).
  * [Specific Gitlab Runner in GKE](https://tortuemat.gitlab.io/blog/2017/07/26/gitlab-runner-kubernetes/).
  * [Playing with gitlab-ci pipelines](https://blog.matters.tech/playing-with-gitlab-ci-pipelines-b4c6dbeaec32).
* 07 - Liberando Productos: Medir Impacto y Gestionar Incidencias.
  * [Read the Docs](https://readthedocs.org/).
  * [Install Go Debian 9](https://www.digitalocean.com/community/tutorials/how-to-install-go-on-debian-9).
  * [Prometheus Metric and label naming](https://prometheus.io/docs/practices/naming/).
  * [Prometheus Node Exporter](https://github.com/prometheus/node_exporter).
  * [Google CAdvisor Containers](https://github.com/google/cadvisor).
  * [Kubernetes kube-state-metrics](https://github.com/kubernetes/kube-state-metrics).
  * [M3DB](https://www.m3db.io/).
  * [Thanos: Highly Prometheus Long Term Storage](https://github.com/thanos-io/thanos).
  * [CNCF Jaeger Distributed Tracing Platform](https://github.com/jaegertracing/jaeger).
  * [Operadores Kubernetes](https://www.youtube.com/watch?v=KP1IYRK5UNo).
  * [SRE DevOps, SLIs, SLOs, SLAs](https://www.youtube.com/watch?v=tEylFyxbDLE).
  * [Availability Calculator](https://dastergon.gr/availability-calculator/).
  * [Error Budget Calculator](https://dastergon.gr/error-budget-calculator/).
  * [Calculate Net Promoter Score](https://customergauge.com/blog/how-to-calculate-the-net-promoter-score).
  * [Eliminating Toil](https://landing.google.com/sre/sre-book/chapters/eliminating-toil/).
  * [Server Resilience Patterns](https://speakerdeck.com/slok/resilience-patterns-server-edition).
  * [Resilience Patterns Server Edition](https://www.youtube.com/watch?v=M_B0CnL88ZE).
  * [Netflix/Hystrix Resilience](https://github.com/Netflix/Hystrix).
  * [Resilience Exponential Backoff (Client)](https://en.wikipedia.org/wiki/Exponential_backoff).
  * [Principles of Chaos Engineering](http://principlesofchaos.org/?lang=ENcontent).
  * [Chaos Monkey Resiliency](https://github.com/Netflix/chaosmonkey).
  * [PagerDuty Real-Time Operations Incident Response On-Call](https://www.pagerduty.com/).
  * [Opsgenie Powerful Alerting On-Call Management](https://www.atlassian.com/software/opsgenie).
  * [Choose an Open Source License](https://choosealicense.com/).
  * [Open Source Guide](https://opensource.guide/).
  * [CNCF Cloud Native Interactive Landscape](https://landscape.cncf.io/).
  * [Monitoring Resource Metrics Prometheus](https://www.replex.io/blog/kubernetes-in-production-the-ultimate-guide-to-monitoring-resource-metrics).
* 08 - Proyecto Final.
  * [OBS: Open Broadcaster Software](https://obsproject.com/).
  * [Shotcut](https://shotcut.org/).
  * [Principio KISS](https://es.wikipedia.org/wiki/Principio_KISS).
  * [MVP](https://es.wikipedia.org/wiki/Producto_viable_m%C3%ADnimo).
