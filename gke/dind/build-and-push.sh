#!/bin/bash

ARG1=${1:-19.03.4-dind-0.0.1}

docker build -t ureure/ure-dind-kubectl:$ARG1 .
docker push ureure/ure-dind-kubectl:$ARG1
