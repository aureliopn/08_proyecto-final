#!/bin/bash

helm install --name prometheus stable/prometheus-operator --namespace=monitoring --version v8.3.0 -f ./values.yaml
kubectl apply -f monitoring-ingress.yaml --namespace=monitoring
