#!/bin/bash

sudo apt-get update
sudo apt-get install apt-transport-https -y

# Registrar clave y fuente de Microsoft
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
wget -q https://packages.microsoft.com/config/debian/9/prod.list
sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
sudo chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
sudo chown root:root /etc/apt/sources.list.d/microsoft-prod.list

# Instalar SDK y entorno de ejecución de .NET Core
sudo apt-get update
sudo apt-get install dotnet-sdk-2.2 dotnet-runtime-2.2 -y

# Instalar FileBeat
wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-6.2.4-amd64.deb
sudo dpkg -i filebeat-6.2.4-amd64.deb
# Establecer logging.selectors: ["*"], entrada logs de Redis, y salida a Logstash
sudo cp /vagrant/counter/filebeat.yml /etc/filebeat/filebeat.yml
sudo service filebeat start
sudo systemctl enable filebeat
rm filebeat-6.2.4-amd64.deb

# Instalar Redis
sudo apt-get install redis-server -y

# Establecer la configuración de la base de datos
sudo systemctl stop redis
sudo cp /vagrant/counter/redis.conf /etc/redis/redis.conf
sudo systemctl start redis

# Instalar Git
sudo apt-get install git -y

# Clonar netcore-counter, compilar, establecer la configuración, y ejecutar
git clone https://gitlab.com/aureliopn/netcore-counter
cd netcore-counter/solution
dotnet restore
cd src/Counter.Web
dotnet publish -c Release
cd publish/netcoreapp2.2/publish
sudo cp /vagrant/counter/appsettings.json ./appsettings.json
sudo cp /vagrant/counter/nlog.config ./nlog.config
dotnet Counter.Web.dll &

# Mostrar recursos de almacenamiento
df

exit 0
