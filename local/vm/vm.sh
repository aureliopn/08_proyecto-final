#!/bin/bash

sudo apt-get update && sudo apt-get install curl redis-server -y
sudo systemctl stop redis
sudo cp redis.conf /etc/redis/redis.conf
sudo chown redis:redis /etc/redis/redis.conf
sudo systemctl start redis
chmod 700 instalar_dotnet_core.sh
./instalar_dotnet_core.sh
git clone https://gitlab.com/aureliopn/netcore-counter
cd netcore-counter/solution
dotnet restore
cd src/Counter.Web
dotnet publish -c Release
cd publish/netcoreapp2.2/publish
sudo cp ../../../../../../../appsettings.json ./appsettings.json
dotnet Counter.Web.dll

exit 0
